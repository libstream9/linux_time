#include <stream9/linux/time/time.hpp>

#include <stream9/linux/error.hpp>

#include <limits>

namespace stream9::linux {

::time_t
time()
{
    ::time_t t;

    auto const rc = ::time(&t);
    if (rc == -1) {
        throw error {
            "time()",
            linux::make_error_code(errno),
        };
    }

    return t;
}

struct ::timespec
clock_gettime(::clockid_t clock_id)
{
    struct ::timespec result;

    auto const rc = ::clock_gettime(clock_id, &result);
    if (rc == -1) {
        throw error {
            "clock_gettime()",
            linux::make_error_code(errno),
        };
    }

    return result;
}

} // namespace stream9::linux
