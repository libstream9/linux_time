#include <stream9/linux/time/string.hpp>

#include <stream9/linux/time/json.hpp>

#include <time.h>

#include <stream9/cstring_view.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/strings/stream.hpp>

namespace stream9::linux {

template<std::integral T1, std::integral T2>
void
validate_value(cstring_view const name,
               T1 const v,
               T2 const min, T2 const max)
{
    if (v < min || v > max) {
        throw error {
            std::make_error_code(std::errc::value_too_large), {
                { name, v },
                { "min", min },
                { "max", max },
            }
        };
    }
}

static void
validate_tm(struct ::tm const& t)
{
    try {
        validate_value("tm_sec", t.tm_sec, 0, 60);
        validate_value("tm_min", t.tm_min, 0, 59);
        validate_value("tm_hour", t.tm_hour, 0, 23);
        validate_value("tm_mday", t.tm_mday, 1, 31);
        validate_value("tm_mon", t.tm_mon, 0, 11);
        // we won't check tm_year
        validate_value("tm_wday", t.tm_wday, 0, 6);
        validate_value("tm_yday", t.tm_yday, 0, 365);
        // we won't check tm_isdst
    }
    catch (...) {
        rethrow_error();
    }
}

static void
validate_time_t(cstring_view const name, ::time_t const t)
{
    if (t < 0) {
        throw error {
            std::make_error_code(std::errc::value_too_large), {
                { name, t },
                { "min", 0 },
            }
        };
    }
}

static void
validate_timespec(struct ::timespec const& t)
{
    validate_time_t("tv_sec", t.tv_sec);
    validate_value("tv_nsec", t.tv_nsec, 0, 999999999);
}

static std::string
to_duration(struct ::tm const& t,
            long nsec = 0)
{
    try {
        validate_tm(t);

        std::string s;

        stream9::strings::ostream os { s };

        os << 'P';
        if (t.tm_year != 0) {
            os << t.tm_year << 'Y';
        }
        if (t.tm_mon != 0) {
            os << t.tm_mon << 'M';
        }
        if (t.tm_mday > 1) {
            os << t.tm_mday - 1 << 'D';
        }
        if (t.tm_hour || t.tm_min || t.tm_sec || nsec) {
            os << 'T';
            if (t.tm_hour != 0) {
                os << t.tm_hour << 'H';
            }
            if (t.tm_min != 0) {
                os << t.tm_min << 'M';
            }
            if (t.tm_sec != 0 || nsec != 0) {
                os << t.tm_sec;
                if (nsec) {
                    os << '.';
                    long f = nsec;
                    long x = 1000000000 / 10;

                    while (f) {
                        auto q = f / x;
                        auto r = f % x;

                        os << q;

                        f = r;
                        x /= 10;
                    }
                }
                os << 'S';
            }
        }
        else {
            os << "T0S";
        }

        return s;
    }
    catch (...) {
        rethrow_error();
    }
}

std::string
to_iso8601_duration(struct ::tm const& t)
{
    try {
        return to_duration(t);
    }
    catch (...) {
        rethrow_error({
            { "t", t },
        });
    }
}

std::string
to_iso8601_duration(struct ::timespec const& ts)
{
    try {
        validate_timespec(ts);

        struct ::tm tmbuf {};

        auto* const tm = ::gmtime_r(&ts.tv_sec, &tmbuf);
        if (tm == nullptr) {
            throw error {
                "gmtime_r()",
                linux::make_error_code(errno),
            };
        }
        tm->tm_year -= 70;

        return to_duration(*tm, ts.tv_nsec);
    }
    catch (...) {
        rethrow_error({
            { "ts", ts },
        });
    }
}

std::string
to_iso8601_duration(::time_t const t)
{
    try {
        validate_time_t("t", t);

        struct ::tm tmbuf {};

        auto* const tm = ::gmtime_r(&t, &tmbuf);
        if (tm == nullptr) {
            throw error {
                "gmtime_r()",
                linux::make_error_code(errno),
            };
        }
        tm->tm_year -= 70;

        return to_iso8601_duration(*tm);
    }
    catch (...) {
        rethrow_error({
            { "t", t },
        });
    }
}

std::string
to_iso8601_datetime(struct ::tm const& t)
{
    try {
        char buf[256];

        auto const n = ::strftime(buf, sizeof(buf), "%FT%T%z", &t);
        if (n == 0) {
            throw error {
                "strftime()",
                std::make_error_code(std::errc::value_too_large)
            };
        }

        return buf;
    }
    catch (...) {
        rethrow_error({
            { "t", t },
        });
    }
}

std::string
to_iso8601_datetime(struct ::timespec const& t)
{
    try {
        struct ::tm tmbuf;

        auto* const tm = ::localtime_r(&t.tv_sec, &tmbuf);
        if (tm == nullptr) {
            throw error {
                "local_time_r()",
                linux::make_error_code(errno),
            };
        }

        return to_iso8601_datetime(*tm);
    }
    catch (...) {
        rethrow_error({
            { "t", t },
        });
    }
}

std::string
to_iso8601_datetime(::time_t const t)
{
    try {
        struct ::tm tmbuf;

        auto* const tm = ::localtime_r(&t, &tmbuf);
        if (tm == nullptr) {
            throw error {
                "local_time_r()",
                linux::make_error_code(errno),
            };
        }

        return to_iso8601_datetime(*tm);
    }
    catch (...) {
        rethrow_error({
            { "t", t },
        });
    }
}

} // namespace stream9::linux
