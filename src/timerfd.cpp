#include <stream9/linux/time/timerfd.hpp>

#include <stream9/linux/time/string.hpp>

#include <time.h>

#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/read.hpp>
#include <stream9/strings/stream.hpp>

namespace stream9::json {

void
tag_invoke(value_from_tag, value& jv, struct ::itimerspec const& v)
{
    auto& obj = jv.emplace_object();

   obj["it_interval"] = linux::to_iso8601_duration(v.it_interval);
   obj["it_value"] = linux::to_iso8601_duration(v.it_value);
}

} // namespace stream9::json

namespace stream9::linux {

static std::string
clockid_to_symbol(int const clockid)
{
    std::string s;

    switch (clockid) {
        case CLOCK_REALTIME: s = "CLOCK_REALTIME"; break;
        case CLOCK_MONOTONIC: s = "CLOCK_MONOTONIC"; break;
        case CLOCK_BOOTTIME: s = "CLOCK_BOOTTIME"; break;
        case CLOCK_REALTIME_ALARM: s = "CLOCK_REALTIME_ALARM"; break;
        case CLOCK_BOOTTIME_ALARM: s = "CLOCK_BOOTTIME_ALARM"; break;
    }

    if (s.empty()) {
        using stream9::strings::operator<<;
        s << "unknown (" << clockid << ")";
    }

    return s;
}

static std::string
flags_to_symbol(int const flags)
{
    std::string s;

    switch (flags) {
        case TFD_NONBLOCK: s = "TFD_NONBLOCK"; break;
        case TFD_CLOEXEC: s = "TFD_CLOEXEC"; break;
        case TFD_TIMER_ABSTIME: s = "TFD_TIMER_ABSTIME"; break;
        case TFD_TIMER_CANCEL_ON_SET: s = "TFD_TIMER_CANCEL_ON_SET"; break;
    }

    if (s.empty()) {
        using stream9::strings::operator<<;
        s << "unknown (" << flags << ")";
    }

    return s;
}

static int
make_timerfd(int const clockid, int const flags)
{
    auto const rc = ::timerfd_create(clockid, flags);
    if (rc == -1) {
        throw error {
            "timerfd_create()",
            linux::make_error_code(errno),
        };
    }

    return rc;
}

/*
 * class timerfd
 */
timerfd::
timerfd(int const clockid, int const flags)
    try : m_fd { make_timerfd(clockid, flags) }
{}
catch (...) {
    rethrow_error({
        { "fd", m_fd },
        { "clockid", clockid_to_symbol(clockid) },
        { "flags", flags_to_symbol(flags) },
    });
}

struct ::itimerspec timerfd::
gettime() const
{
    struct ::itimerspec result;

    auto const rc = ::timerfd_gettime(m_fd, &result);
    if (rc == -1) {
        throw error {
            "timerfd_gettime()",
            linux::make_error_code(errno), {
                { "fd", m_fd },
            }
        };
    }

    return result;
}

std::uint64_t timerfd::
read()
{
    try {
        std::uint64_t v {};

        auto const n = linux::read(m_fd, v).or_throw();
        if (n != sizeof(v)) {
            json::object cxt {
                { "timerfd", m_fd },
            };
            if (n != -1) {
                cxt["n"] = n.value();
            }

            throw error {
                "read()",
                linux::make_error_code(errno),
                std::move(cxt)
            };
        }

        return v;
    }
    catch (...) {
        rethrow_error();
    }
}

outcome<std::uint64_t, int> timerfd::
read_nothrow() noexcept
{
    std::uint64_t v {};

    auto const o_n = linux::read(m_fd, v);
    if (o_n) {
        assert(*o_n == sizeof(v));
        return { st9::value_tag(), v };
    }
    else {
        return { st9::error_tag(), o_n.error() };
    }
}

struct ::itimerspec timerfd::
settime(int const flags, struct ::itimerspec const& new_value)
{
    struct ::itimerspec old_value {};

    auto const rc = ::timerfd_settime(m_fd, flags, &new_value, &old_value);
    if (rc == -1) {
        throw error {
            "timerfd_settime()",
            linux::make_error_code(errno), {
                { "fd", m_fd },
                { "flags", flags_to_symbol(flags) },
                { "new_value", new_value },
            }
        };
    }

    return old_value;
}

void
disarm(timerfd& tfd)
{
    try {
        struct ::itimerspec value { {}, {} };

        tfd.settime(0, value);
    }
    catch (...) {
        rethrow_error({
            { "timerfd", tfd.fd() },
        });
    }
}

} // namespace stream9::linux
