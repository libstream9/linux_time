#include <stream9/linux/time/json.hpp>

namespace stream9::json {

void
tag_invoke(value_from_tag,
           value& jv,
           struct ::tm const& t)
{
    auto& obj = jv.emplace_object();

    obj["tm_sec"] = t.tm_sec;
    obj["tm_min"] = t.tm_min;
    obj["tm_hour"] = t.tm_hour;
    obj["tm_mday"] = t.tm_mday;
    obj["tm_mon"] = t.tm_mon;
    obj["tm_year"] = t.tm_year;
    obj["tm_wday"] = t.tm_wday;
    obj["tm_yday"] = t.tm_yday;
    obj["tm_isdst"] = t.tm_isdst;
}

void
tag_invoke(value_from_tag,
           value& jv,
           struct ::timespec const& t)
{
    jv = json::object {
        { "tv_sec", t.tv_sec },
        { "tv_nsec", t.tv_nsec },
    };
}

} // namespace stream9::json
