#ifndef STREAM9_LINUX_TIME_HPP
#define STREAM9_LINUX_TIME_HPP

#include <time.h>

#include <chrono>

#include <stream9/linux/error.hpp>

namespace stream9::linux {

::time_t time();

struct ::timespec
clock_gettime(::clockid_t clock_id);

} // namespace stream9::linux

#endif // STREAM9_LINUX_TIME_HPP
