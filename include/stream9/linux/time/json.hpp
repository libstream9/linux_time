#ifndef STREAM9_LINUX_TIME_JSON_HPP
#define STREAM9_LINUX_TIME_JSON_HPP

#include <time.h>

#include <stream9/json.hpp>

namespace stream9::json {

void
tag_invoke(value_from_tag,
           value&, struct ::tm const&);

void
tag_invoke(value_from_tag,
           value&, struct ::timespec const&);

} // namespace stream9::json

#endif // STREAM9_LINUX_TIME_JSON_HPP
