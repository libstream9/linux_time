#ifndef STREAM9_LINUX_TIME_CHRONO_HPP
#define STREAM9_LINUX_TIME_CHRONO_HPP

#include <time.h>

#include <chrono>

#include <stream9/linux/error.hpp>

namespace stream9::linux {

/*
 * struct timespec <=> std::chrono::duration
 */
struct ::timespec
to_timespec(std::chrono::nanoseconds);

template<typename Rep = std::int64_t,
         typename Period = std::nano >
std::chrono::duration<Rep, Period>
to_duration(struct ::timespec&);

/*
 * time_t <=> std::chrono::duration
 */
template<typename Rep, typename Period>
::time_t
to_time_t(std::chrono::duration<Rep, Period>);

template<typename Clock, typename Duration>
::time_t
to_time_t(std::chrono::time_point<Clock, Duration>);

template<typename Rep = ::time_t,
         typename Period = std::ratio<1> >
std::chrono::duration<Rep, Period>
to_duration(::time_t);

/*
 * struct tm <=> std::chrono::time_point
 */
template<typename Clock = std::chrono::system_clock,
         typename Duration = std::chrono::seconds >
std::chrono::time_point<Clock, Duration>
to_time_point(struct ::tm&);

template<typename Clock, typename Duration>
struct ::tm
gmtime(std::chrono::time_point<Clock, Duration> const&);

template<typename Clock, typename Duration>
struct ::tm
localtime(std::chrono::time_point<Clock, Duration> const&);

} // namespace stream9::linux

#endif // STREAM9_LINUX_TIME_CHRONO_HPP

#include "chrono.ipp"
