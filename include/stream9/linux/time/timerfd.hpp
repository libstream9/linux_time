#ifndef STREAM9_LINUX_TIMERFD_HPP
#define STREAM9_LINUX_TIMERFD_HPP

#include "chrono.hpp"

#include <sys/timerfd.h>

#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/outcome.hpp>

namespace stream9::linux {

class timerfd
{
public:
    // essential
    explicit timerfd(int clockid, int flags = 0);

    timerfd(timerfd const&) = delete;
    timerfd& operator=(timerfd const&) = delete;
    timerfd(timerfd&&) = default;
    timerfd& operator=(timerfd&&) = default;

    // accessor
    fd_ref fd() const noexcept { return m_fd; }

    // query
    struct ::itimerspec gettime() const;

    std::uint64_t read();
    outcome<std::uint64_t, int> read_nothrow() noexcept;

    // modifier
    struct ::itimerspec
        settime(int flags, struct ::itimerspec const& new_value);

    // conversion
    operator fd_ref () const noexcept { return fd(); }

    // comparison
    bool operator==(fd_ref const f) const noexcept { return f == fd(); }

private:
    class fd m_fd;
};

template<typename Rep, typename Period>
struct ::itimerspec
set_oneshot(timerfd& tfd, std::chrono::duration<Rep, Period> const& v)
{
    try {
        auto const tv = to_timespec(v);
        struct ::itimerspec value { tv, {} };

        return tfd.settime(0, value);
    }
    catch (...) {
        rethrow_error({
            { "timerfd", tfd.fd() },
            { "expire", v }
        });
    }
}

template<typename Clock, typename Duration>
struct ::itimerspec
set_oneshot(timerfd& tfd, std::chrono::time_point<Clock, Duration> const& v)
{
    try {
        auto const tv = to_timespec(v.time_since_epoch());
        struct ::itimerspec value { tv, {} };

        return tfd.settime(TFD_TIMER_ABSTIME, value);
    }
    catch (...) {
        rethrow_error({
            { "timerfd", tfd.fd() },
            { "expire" ,v }
        });
    }
}

template<typename Rep, typename Period>
struct ::itimerspec
set_interval(timerfd& tfd, std::chrono::duration<Rep, Period> const& v)
{
    try {
        auto const tv = to_timespec(v);
        struct ::itimerspec value { tv, tv };

        return tfd.settime(0, value);
    }
    catch (...) {
        rethrow_error({
            { "timerfd", tfd.fd() },
            //{ "expire" ,v }
        });
    }
}

void disarm(timerfd& tfd);

} // namespace stream9::linux

namespace stream9::json {

struct value_from_tag;
class value;

void tag_invoke(value_from_tag, value&, struct ::itimerspec const&);

} // namespace stream9::json

#endif // STREAM9_LINUX_TIMERFD_HPP
