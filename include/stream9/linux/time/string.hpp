#ifndef STREAM9_LINUX_TIME_STRING_HPP
#define STREAM9_LINUX_TIME_STRING_HPP

#include <time.h>

#include <string>

namespace stream9::linux {

std::string
to_iso8601_duration(::time_t);

std::string
to_iso8601_duration(struct ::timespec const&);

std::string
to_iso8601_duration(struct ::tm const&);

std::string
to_iso8601_datetime(::time_t);

std::string
to_iso8601_datetime(struct ::timespec const&);

std::string
to_iso8601_datetime(struct ::tm const&);

} // namespace stream9::linux

#endif // STREAM9_LINUX_TIME_STRING_HPP
