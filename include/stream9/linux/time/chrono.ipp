#ifndef STREAM9_LINUX_TIME_CHRONO_IPP
#define STREAM9_LINUX_TIME_CHRONO_IPP

namespace stream9::linux {

/*
 * struct timespec <=> std::chrono::duration
 */
inline struct ::timespec
to_timespec(std::chrono::nanoseconds const d)
{
    struct ::timespec tv;

    auto const sec = d.count() / std::nano::den;

    if (sec > std::numeric_limits<::time_t>::max()) {
        throw error {
            std::make_error_code(std::errc::value_too_large), {
                { "d", d.count() }
            }
        };
    }
    tv.tv_sec = sec;
    tv.tv_nsec = d.count() % std::nano::den;

    return tv;
}

template<typename Rep/* = std::int64_t*/,
         typename Period/* = std::nano*/ >
std::chrono::duration<Rep, Period>
to_duration(struct ::timespec& ts)
{
    namespace C = std::chrono;

    C::seconds sec { ts.tv_sec };
    C::nanoseconds nsec { ts.tv_nsec };

    return sec + nsec;
}

/*
 * time_t <=> std::chrono::duration
 */
template<typename Rep, typename Period>
::time_t
to_time_t(std::chrono::duration<Rep, Period> const d)
{
    std::chrono::duration<::time_t> sec { d };

    return sec.count();
}

template<typename Clock, typename Duration>
::time_t
to_time_t(std::chrono::time_point<Clock, Duration> const tp)
{
    return to_time_t(tp.time_since_epoch());
}

template<typename Rep/* = ::time_t*/,
         typename Period/* = std::ratio<1>*/ >
std::chrono::duration<Rep, Period>
to_duration(::time_t const t)
{
    return std::chrono::duration<Rep, Period> { t };
}

/*
 * struct tm <=> std::chrono::time_point
 */
template<typename Clock/* = std::chrono::system_clock*/,
         typename Duration/* = std::chrono::seconds*/ >
std::chrono::time_point<Clock, Duration>
to_time_point(struct ::tm& tm)
{
    auto const t = ::mktime(&tm);

    return std::chrono::time_point<Clock, Duration> {
        std::chrono::seconds { t }
    };
}

template<typename Clock, typename Duration>
struct ::tm
gmtime(std::chrono::time_point<Clock, Duration> const& tp)
{
    struct ::tm tm {};
    std::chrono::duration<::time_t> sec = tp.time_since_epoch();

    auto const c = sec.count();
    auto const r = ::gmtime_r(&c, &tm);
    if (r == nullptr) {
        throw error {
            "gmtime_r()",
            linux::make_error_code(errno),
        };
    }

    return tm;
}

template<typename Clock, typename Duration>
struct ::tm
localtime(std::chrono::time_point<Clock, Duration> const& tp)
{
    struct ::tm tm {};
    std::chrono::duration<::time_t> sec = tp.time_since_epoch();

    auto const c = sec.count();
    auto const r = ::localtime_r(&c, &tm);
    if (r == nullptr) {
        throw error {
            "localtime_r()",
            linux::make_error_code(errno),
        };
    }

    return tm;
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_TIME_CHRONO_IPP
